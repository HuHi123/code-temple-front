class ApiCaller {

    constructor(url) {
        this.url = url;
    }


    static setToken(token) {

        window.localStorage.setItem('token', token);

    }

    static getToken() {

        return window.localStorage.getItem('token');

    }

    async jfetch(method, resource, token = false, body = undefined, queries=undefined) {

        return new Promise((resolve, reject) => {
            let jwt = undefined;
            if (token)
                jwt = 'Bearer ' + ApiCaller.getToken();
            let url = this.url;
            url += resource;
            if (queries && Array.isArray(queries)) {
                url += "?";
                queries.forEach(el => {
                    url += el.key;
                    url += "=";
                    url += el.value;
                    url += "&";
                })
            }
            fetch(url, {
                method: method,
                headers: {
                    'Content-Type': 'application/json',
                    Authorization: jwt
                    //"Access-Control-Allow-Methods": "GET, POST, PUT, OPTIONS"
                    //'Content-Type': 'application/x-www-form-urlencoded',
                },
                body: JSON.stringify(body)
            })
                .then(response => {
                    //console.log("here"+response.toSource());
                    if (response.status != 200)
                        return Promise.reject(2);
                    else
                        return response.json()
                            .catch(e=>{
                                return {};
                            })
                })
                .then((res) => {
                    resolve(res);
                })
                .catch(err => {
                    reject(err);
                });

        });

    }

    async signup(name, email, password) {
        return new Promise((resolve, reject) => {
            let body = {
                name: name,
                email: email,
                password: password
            };
            this.jfetch("POST", "/signup", false, body)
                .then(res => {
                    ApiCaller.setToken(res.token);
                    resolve(res);
                })
                .catch(err => {
                    reject(err);
                })
        })

    }

    async login(email, password) {
        return new Promise((resolve, reject) => {
            let body = {
                email: email,
                password: password
            };
            this.jfetch("POST", "/login", false, body)
                .then(res => {
                    ApiCaller.setToken(res.token);
                    resolve(res);
                })
                .catch(err => {
                    reject(err);
                })
        })

    }

    async dashboard() {
        return new Promise((resolve, reject) => {

            this.jfetch("GET", "/project/dashboard", true)
                .then(res => {
                    resolve(res);
                })
                .catch(err => {
                    reject(err);
                })
        })

    }

    logout() {

        ApiCaller.setToken("");

    }

    async createProject(project) {
        return new Promise((resolve,reject)=>{
            let body = {
                project: project
            };
            this.jfetch("POST", "/project/create", true,body)
                .then(res => {
                    resolve(res);
                })
                .catch(err => {
                    reject(err);
                })
        })

    }

    async deleteProject(projectId) {
        return new Promise((resolve,reject)=>{
            let body = {
                projectId: projectId
            };
            this.jfetch("POST", "/project/delete", true,body)
                .then(res => {
                    resolve(res);
                })
                .catch(err => {
                    reject(err);
                })
        })

    }

    async editProject(projectId,name) {
        return new Promise((resolve,reject)=>{
            let body = {
                projectId: projectId,
                name: name,
            };
            this.jfetch("POST", "/project/edit", true,body)
                .then(res => {
                    resolve(res);
                })
                .catch(err => {
                    reject(err);
                })
        })

    }

    async addParticipant(participant,projectId) {
        return new Promise((resolve,reject)=>{
            let body = {
                projectId: projectId,
                participant: participant
            };
            this.jfetch("POST", "/project/editParticipants", true,body)
                .then(res => {
                    resolve(res);
                })
                .catch(err => {
                    reject(err);
                })
        })

    }

    async acceptProject(projectId) {
        return new Promise((resolve,reject)=>{
            let body = {
                projectId: projectId
            };
            this.jfetch("POST", "/project/accept", true,body)
                .then(res => {
                    resolve(res);
                })
                .catch(err => {
                    reject(err);
                })
        })

    }

    async rejectProject(projectId) {
        return new Promise((resolve,reject)=>{
            let body = {
                projectId: projectId
            };
            this.jfetch("POST", "/project/reject", true,body)
                .then(res => {
                    resolve(res);
                })
                .catch(err => {
                    reject(err);
                })
        })

    }

    async enterProject(projectId) {
        return new Promise((resolve,reject)=>{
            this.jfetch("GET", "/file/project/"+projectId, true)
                .then(res => {
                    resolve(res);
                })
                .catch(err => {
                    reject(err);
                })
        })

    }

    async newFile(projectId,parentId,name,type) {
        let body ={
            projectId:projectId,
            parentId:parentId,
            name:name,
            type:type
        };
        return new Promise((resolve,reject)=>{
            this.jfetch("POST", "/file/create", true,body)
                .then(res => {
                    resolve(res);
                })
                .catch(err => {
                    reject(err);
                })
        })

    }

    async deleteFile(fileId,type) {
        let body ={
            fid:fileId,
            type:type
        };
        return new Promise((resolve,reject)=>{
            this.jfetch("POST", "/file/delete", true,body)
                .then(res => {
                    resolve(res);
                })
                .catch(err => {
                    reject(err);
                })
        })

    }

    async renameFile(fileId,type,name,projectId) {
        if(projectId){

            return new Promise((resolve,reject)=>{
                let body = {
                    projectId: projectId,
                    name: name,
                };
                console.log('ss');
                this.jfetch("POST", "/project/edit2", true,body)
                    .then(res => {
                        console.log('def');
                        resolve(res);
                    })
                    .catch(err => {
                        console.log('defcc');
                        reject(err);
                    })
            })
        }else {

            return new Promise((resolve, reject) => {
                let body = {
                    fid: fileId,
                    name: name,
                    type: type
                };
                this.jfetch("POST", "/file/changeName", true, body)
                    .then(res => {
                        resolve(res);
                    })
                    .catch(err => {
                        reject(err);
                    })
            })
        }

    }

    async changeDefault(fileId) {
        let body ={
            fid:fileId,
        };
        return new Promise((resolve,reject)=>{
            this.jfetch("POST", "/file/changeDefault", true,body)
                .then(res => {
                    resolve(res);
                })
                .catch(err => {
                    reject(err);
                })
        })

    }

    async getFile(fileId) {
        return new Promise((resolve,reject)=>{
            this.jfetch("GET", "/file/"+fileId, true)
                .then(res => {
                    resolve(res);
                })
                .catch(err => {
                    reject(err);
                })
        })

    }

    async editFile(fileId,content) {
        let body ={
            fid:fileId,
            content:content
        };
        return new Promise((resolve,reject)=>{
            this.jfetch("POST", "/file/edit", true,body)
                .then(res => {
                    resolve(res);
                })
                .catch(err => {
                    reject(err);
                })
        })

    }



}

export default ApiCaller;

/*
    router.post('/delete',(req,res)=>{
        File.deleteFile(req.user.id,req.body.fid,req.body.type)
    router.post('/create',(req,res)=>{
        File.newFile(req.user.id,req.body.projectId,req.body.parentId,req.body.name,req.body.type)
    router.post('/changeName',(req,res)=>{

    File.changeName(req.user.id,req.body.fid,req.body.type,req.body.name)


        router.post('/changeDefault',(req,res)=>{
    File.changeDefault(req.user.id,req.body.fid)
router.get('/:id',(req,res)=>{
    File.getFile(req.user.id,req.params.id)
router.post('/edit',(req,res)=>{
    File.editFile(req.user.id,req.body.fid,req.body.content)
*/