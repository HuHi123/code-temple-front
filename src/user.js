
import React from 'react';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Link from '@material-ui/core/Link';
import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import ErrorIcon from '@material-ui/icons/Error';
import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';

import { createMuiTheme,responsiveFontSizes } from '@material-ui/core/styles';
import { ThemeProvider } from '@material-ui/core/styles';
import InputAdornment from "@material-ui/core/InputAdornment";
import IconButton from "@material-ui/core/IconButton";

const theme = createMuiTheme({
    palette: {
        type: 'dark',
        primary: {
            main: '#9EA2B3',
        },
    }
});


theme.typography.h2 = {
    fontSize: '3rem',
    '@media (min-width:300px)': {
        fontSize: '2.75rem',
    },
    [theme.breakpoints.up('sm')]: {
        fontSize: '4rem',
    },
    [theme.breakpoints.up('md')]: {
        fontSize: '6rem',
    },
    // [theme.breakpoints.up('lg')]: {
    //     fontSize: '8.2rem',
    // },
    // [theme.breakpoints.up('xl')]: {
    //     fontSize: '8.3rem',
    // },
};

//const theme2 = responsiveFontSizes(theme);


function Copyright() {
    return (
        <Typography variant="body2" color="textSecondary" align="center">
            {'Copyright © '}
            <Link color="inherit" href="https://material-ui.com/">
                Your Website
            </Link>{' '}
            {new Date().getFullYear()}
            {'.'}
        </Typography>
    );
}

const useStyles = makeStyles(theme => ({

    '@global': {
        body: {
            backgroundColor: '#3c3c3d',
        },
    },
    paper: {
        marginTop: '4em',
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
    avatar: {
        margin: theme.spacing(1),
        backgroundColor: theme.palette.secondary.main,
    },
    form: {
        width: '100%', // Fix IE 11 issue.
        marginTop: '3em',
    },



}));

export default function SignIn(props) {
    const [login,setLogin] = React.useState(true);

    const [info,setInfo] = React.useState({
        name:"",
        email:"",
        password:""
    });
    const [isValid,setIsValid] = React.useState({
        name:false,
        email:false,
        password:false
    });
    const [showPassword,setShowPassword] = React.useState(false);

    function validateEmail(email){
        let regex = new RegExp('(([^<>()\\[\\]\\\\.,;:\\s@"]+(\\.[^<>()\\[\\]\\\\.,;:\\s@"]+)*)|(".+"))@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))');
        return regex.test(email);
    }

    function validatePassword(password){
        let regex =new RegExp('(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[#$^+=!*()@%&]).{8,100}');
        return regex.test(password);
    }

    function validateName(name){
        let regex =new RegExp('^[A-Z]?[a-z]+( [A-Z]?[a-z]+)?$');
        return regex.test(name);
    }

    const classes = useStyles();

    return (
        <div>



            <Container component="main" maxWidth="xs">

                <CssBaseline />
                <div className={classes.paper}>


                    <ThemeProvider theme={theme}>
                    <div style={{
                        display: 'flex',
                        flexDirection: 'row',
                        justifyContent: 'center',
                    }}>
                        <Typography variant="h2" style={{
                            fontFamily: 'Blanka',
                            color: '#9EA2B3'
                        }}>
                            {'Code'}
                        </Typography>
                        <Typography variant="h2" style={{
                            fontFamily: 'Blanka',
                            color: '#8C769E'
                        }}>
                            {' Temple '}
                        </Typography>
                        <Typography variant="h2" style={{
                            fontFamily: 'Blanka',
                            color: '#CDB762'
                        }}>
                            {'</>'}
                        </Typography>
                    </div>
                    </ThemeProvider>

                    <ThemeProvider theme={theme}>
                        <form className={classes.form} >

                            {
                                !login &&
                                <MyTextField
                                    name="name"
                                    label="name"
                                    type="text"
                                    id="name"
                                    autoComplete="name"
                                    value={info.name}
                                    errorMessage={"Invalid name"}
                                    onChange={(text) => {
                                        setInfo({
                                            name: text,
                                            email:info.email,
                                            password:info.password
                                        })
                                    }}
                                    validate={(name) => {
                                        return validateName(name)
                                    }}
                                    onValidityChange={(value)=>{
                                        setIsValid({
                                            name: value,
                                            email: isValid.email,
                                            password: isValid.password
                                        });
                                    }}
                                />
                            }

                            <MyTextField
                                name="email"
                                label="Email Address"
                                type="text"
                                id="email"
                                autoComplete="email"
                                value={info.email}
                                validate={(email)=>{
                                    return validateEmail(email)
                                }}
                                errorMessage={"Invalid email"}
                                onChange={(text)=>{
                                    setInfo({
                                        name: info.name,
                                        email:text,
                                        password:info.password
                                    })
                                }}
                                onValidityChange={(value)=>{
                                    setIsValid({
                                        name: isValid.name,
                                        email: value,
                                        password: isValid.password
                                    });
                                }}
                            />



                            <MyPasswordField
                                name="password"
                                label="password"
                                id="password"
                                value={info.password}
                                validate={(pass)=>{
                                    return validatePassword(pass)
                                }
                                }
                                errorMessage={"8 characters, 1 uppercase, 1 lowercase, 1 special"}
                                onChange={(text)=>{
                                    setInfo({
                                        name: info.name,
                                        email:info.email,
                                        password:text
                                    });
                                }}
                                onValidityChange={(value)=>{
                                    setIsValid({
                                        name: isValid.name,
                                        email: isValid.email,
                                        password: value
                                    });
                                }}

                            />


                            <Button
                                fullWidth
                                variant="contained"
                                color="primary"
                                onClick={()=>{
                                    if(login){
                                        if(props.login && isValid.email && isValid.password){
                                            props.login(info.email,info.password);
                                        }
                                    }else{
                                        if(props.signup && isValid.email && isValid.password && isValid.name ){
                                            props.signup(info.name,info.email,info.password);
                                        }
                                    }

                                }
                                }
                                style={{
                                    marginTop:'1em',
                                    marginBottom:'1em',
                                    padding:'0.6em'
                                }}
                            >
                                {
                                    login ?
                                        "Sign In"
                                        :
                                        "Sign Up"
                                }
                            </Button>


                            <div style={{
                                display: 'flex',
                                flexDirection: 'row',
                                justifyContent: 'center',
                            }}>
                                <Typography>
                                    {
                                        login ?
                                        "Don't have an account?"
                                        :
                                            "Already have an account?"
                                    }
                                </Typography>
                                <Typography color="primary" style={{
                                    cursor: 'pointer',
                                }}
                                onClick={()=>{
                                    if(props.onSwitch)
                                        props.onSwitch();
                                    setLogin(!login);
                                }}
                                >
                                    {
                                        login ?
                                            "Sign Up"
                                            :
                                            "Sign In"
                                    }
                                </Typography>
                            </div>



                        </form>
                    </ThemeProvider>
                </div>
            </Container>
        </div>
    );
}

let MyTextField = (props)=>{
    const [state,setState] = React.useState({
        value: props.value ? props.value : "",
        valid: (props.valid)!==false,
        focus: false
    });

    return (

        (
            !state.valid ?

                <div >
                    <TextField
                        variant="filled"
                        margin="normal"
                        required
                        fullWidth
                        id={props.id}
                        key={props.id}
                        label={props.label}
                        name={props.name}
                        type={props.type}
                        inputProps={props.inputProps}
                        autoComplete={props.autoComplete}
                        error
                        value={state.value}

                        disableAutoFocus={true}
                        disableAutoBlur={true}
                        onChange={(event => {
                            if(props.onChange)
                                props.onChange(event.target.value);
                            setState({
                                value: event.target.value,
                                valid: true,
                                focus: true
                            })
                        })}
                        onBlur={()=>{
                            if(props.validate){
                                if(props.validate(state.value)==false ){
                                    if(props.onValidityChange)
                                        props.onValidityChange(false);
                                    setState({
                                        value: state.value,
                                        valid: false
                                    })
                                }else{
                                    if(props.onValidityChange)
                                        props.onValidityChange(true);
                                }
                            }

                        }}
                    />
                    <Typography color={'error'} style={{fontSize:'1em'}}>
                        {props.errorMessage}
                    </Typography>
                </div>

                :
                <TextField
                    variant="filled"
                    margin="normal"
                    required
                    fullWidth
                    id={props.id}
                    key={props.id}
                    type={props.type}
                    label={props.label}
                    name={props.name}
                    autoComplete={props.autoComplete}
                    value={state.value}
                    autoFocus={state.focus}
                    onChange={(event => {
                        if(props.onChange)
                            props.onChange(event.target.value);

                        setState({
                            value: event.target.value,
                            valid: true
                        })
                    })}
                    onBlur={()=>{
                           if(props.validate){
                               if(props.validate(state.value)==false ){
                                   if(props.onValidityChange)
                                       props.onValidityChange(false);
                                   setState({
                                       value: state.value,
                                       valid: false
                                   })
                               }else{
                                   if(props.onValidityChange)
                                       props.onValidityChange(true);
                               }
                           }

                        }}
                />
        )


    );

};

let MyPasswordField = (props)=>{
    const [state,setState] = React.useState({
        value: props.value ? props.value : "",
        valid: (props.valid)!==false,
        focus: false
    });

    const [showPassword,setShowPassword] = React.useState(false);


    return (

        (
            !state.valid ?

                <div >
                    <TextField
                        variant="filled"
                        margin="normal"
                        required
                        fullWidth
                        id={props.id}
                        key={props.id}
                        label={props.label}
                        name={props.name}
                        type={showPassword? 'text' : 'password' }
                        inputProps={props.inputProps}
                        autoComplete={props.autoComplete}
                        error
                        value={state.value}
                        InputProps={{
                            endAdornment: (
                                <InputAdornment position="end">
                                    <IconButton
                                        aria-label="toggle password visibility"
                                        onClick={()=>{
                                            setShowPassword(!showPassword);
                                        }}
                                        onMouseDown={event => {
                                            event.preventDefault();
                                        }}
                                    >
                                        {showPassword ? <VisibilityOff /> : <Visibility /> }
                                    </IconButton>
                                </InputAdornment>
                            ),
                        }}
                        disableAutoFocus={true}
                        disableAutoBlur={true}
                        onChange={(event => {
                            if(props.onChange)
                                props.onChange(event.target.value);
                            setState({
                                value: event.target.value,
                                valid: true,
                                focus: true
                            })
                        })}
                        onBlur={()=>{
                            if(props.validate){
                                if(props.validate(state.value)==false ){
                                    if(props.onValidityChange)
                                        props.onValidityChange(false);
                                    setState({
                                        value: state.value,
                                        valid: false
                                    })
                                }else{
                                    if(props.onValidityChange)
                                        props.onValidityChange(true);
                                }
                            }

                        }}
                    />
                    <Typography color={'error'} style={{fontSize:'1em'}}>
                        {props.errorMessage}
                    </Typography>
                </div>

                :
                <TextField
                    variant="filled"
                    margin="normal"
                    required
                    fullWidth
                    type={showPassword? 'text' : 'password' }
                    id={props.id}
                    key={props.id}
                    label={props.label}
                    name={props.name}
                    autoComplete={props.autoComplete}
                    value={state.value}
                    InputProps={{
                    endAdornment: (
                        <InputAdornment position="end">
                            <IconButton
                                aria-label="toggle password visibility"
                                onClick={()=>{
                                    setShowPassword(!showPassword);
                                }}
                                onMouseDown={event => {
                                    event.preventDefault();
                                }}
                            >
                                {showPassword ? <VisibilityOff /> : <Visibility /> }
                            </IconButton>
                        </InputAdornment>
                    ),
                }}
                    autoFocus={state.focus}
                    onChange={(event => {
                        if(props.onChange)
                            props.onChange(event.target.value);

                        setState({
                            value: event.target.value,
                            valid: true
                        })
                    })}
                    onBlur={()=>{
                        if(props.validate){
                            if(props.validate(state.value)==false ){
                                if(props.onValidityChange)
                                    props.onValidityChange(false);
                                setState({
                                    value: state.value,
                                    valid: false
                                })
                            }else{
                                if(props.onValidityChange)
                                    props.onValidityChange(true);
                            }
                        }

                    }}
                />
        )


    );

};
















