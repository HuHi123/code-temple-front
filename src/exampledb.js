


const filesys = {
    name: 'Code Templez',
    id:0,
    isDir:true,
    children:[
        {
            id:1,
            name: 'subfile1',
        },
        {
            id:2,
            name: 'subdir1',
            isDir:true,
            children: [
                {
                    id:3,
                    name: 'subfile4',
                },
                {
                    id:4,
                    name: 'subdir2',
                    isDir:true,
                    children: [
                        {
                            id:5,
                            name: 'subfile6',
                        },
                        {
                            id:6,
                            name: 'subfile7',
                        },
                        {
                            id:7,
                            name: 'subfile8',
                        },
                        {
                            id:8,
                            name: 'subdir3',
                            isDir:true,
                            children: [
                                {
                                    id:12,
                                    name: 'subfile9',
                                },
                                {
                                    id:38,
                                    name: 'subdir78',
                                    isDir:true,
                                    children: []
                                },
                                {
                                    id:13,
                                    name: 'subfile10',
                                    isDir:true,
                                    children: [
                                        {
                                            id:14,
                                            name: 'subfile11',
                                        },
                                        {
                                            id:15,
                                            name: 'subfile12s',
                                        },
                                    ]
                                },
                            ]
                        }
                    ]
                },
                {
                    id:9,
                    name: 'subfile5',
                }
            ]
        },
        {
            id:10,
            name: 'subfile2',
        },
        {
            id:11,
            name: 'subfile3',
        },
        {
            id:16,
            name: 'subfile2',
        },
        {
            id:17,
            name: 'subfile3',
        },
        {
            id:18,
            name: 'subfile2',
        },
        {
            id:19,
            name: 'subfile3',
        },
        {
            id:20,
            name: 'subfile2',
        },
        {
            id:21,
            name: 'subfile3',
        },
        {
            id:22,
            name: 'subfile2',
        },
        {
            id:23,
            name: 'subfile3',
        },
        {
            id:24,
            name: 'subfile2',
        },
        {
            id:25,
            name: 'subfile3',
        },
        {
            id:26,
            name: 'subfile2',
        },
        {
            id:27,
            name: 'subfile3',
        },
        {
            id:28,
            name: 'subfile2',
        },
        {
            id:29,
            name: 'subfile3',
        },
        {
            id:30,
            name: 'subfile2',
        },
        {
            id:31,
            name: 'subfile3',
        },
        {
            id:32,
            name: 'subfile2',
        },
        {
            id:33,
            name: 'subfile3',
        },
        {
            id:34,
            name: 'subfile2',
        },
        {
            id:35,
            name: 'subfile3',
        },
        {
            id:36,
            name: 'subfile2',
        },
        {
            id:37,
            name: 'subfile3',
        },

    ]
};

module.exports = {
    filesys:filesys,
};