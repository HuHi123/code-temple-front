import React from "react";
import Button from "@material-ui/core/Button";
import Menu from "@material-ui/core/Menu";
import MenuItem from "@material-ui/core/MenuItem";
import PropTypes from 'prop-types';
import {ThemeProvider,createMuiTheme, fade, makeStyles, withStyles} from '@material-ui/core/styles';
import TreeView from '@material-ui/lab/TreeView';
import TreeItem from '@material-ui/lab/TreeItem';
import Collapse from '@material-ui/core/Collapse';
import Typography from '@material-ui/core/Typography';
import { useSpring, animated } from 'react-spring/web.cjs'; // web.cjs is required for IE 11 support

import FolderOpenIcon from '@material-ui/icons/FolderOpen';
import FolderIcon from '@material-ui/icons/Folder';
import DescriptionIcon from '@material-ui/icons/Description';
import InputBase from "@material-ui/core/InputBase";

const theme = createMuiTheme({
    palette: {
        type: 'dark',
        primary: {
            main: '#9EA2B3',
        },
    }
});

function TransitionComponent(props) {
    const style = useSpring({
        from: { opacity: 0, transform: 'translate3d(20px,0,0)' },
        to: { opacity: props.in ? 1 : 0, transform: `translate3d(${props.in ? 0 : 20}px,0,0)` },
    });

    return (
        <animated.div style={style}>
            <Collapse {...props} />
        </animated.div>
    );
}

TransitionComponent.propTypes = {
    /**
     * Show the component; triggers the enter or exit states
     */
    in: PropTypes.bool,
};

const StyledTreeItem = withStyles(theme => ({
    iconContainer: {
        '& .close': {
            opacity: 0.3,
        },
    },
    group: {
        marginLeft: 12,
        paddingLeft: 12,
        borderLeft: `1px dashed ${fade(theme.palette.text.primary, 0.4)}`,
    },
}))(props => <TreeItem {...props} TransitionComponent={TransitionComponent} />);

//////////////////////////

export default function FileItem(props) {
    //props: onChoose id name categories
    const [anchorEl, setAnchorEl] = React.useState(null);

    const handleClick = event => {
        setAnchorEl(event.currentTarget);

    };

    const handleDoubleClick = ()=>{
        if(props.doubleClick)
            props.doubleClick();
    };

    const handleClose = (event) => {
        setAnchorEl(null);
        if (event.target.innerText) {
            if (props.onChoose) {
                props.onChoose(event.target.innerText);
            }

        }
    };


    let i=0;
    return (
        <div>
            {
                props.isDir===true ?
                    <StyledTreeItem
                        key={props.id}
                        nodeId={"d"+props.id}
                        label={
                            // props.children.length > 0 ?
                                <div
                                    style={{
                                        display: 'flex',
                                        flexDirection: 'row',
                                    } }
                                    onContextMenu={(e)=>{
                                        e.preventDefault();
                                        handleClick(e);
                                    }}
                                >
                                    {
                                        props.children.length < 1 &&
                                        <FolderOpenIcon style={{color: '#ffffff'}}/>
                                    }

                                        <Typography style={{
                                            userSelect : 'none',
                                            WebkitUserSelect: 'none',
                                            MozUserSelect: 'none',
                                            unselectable: 'on',
                                            color: '#ffffff'
                                        }}>{props.name}</Typography>

                                </div>


                        }
                    >

                        {props.children}

                    </StyledTreeItem>
                    :
                    <StyledTreeItem

                        aria-haspopup="true" aria-controls="simple-menu"
                        // onContextMenu={(e)=>{
                        //     e.preventDefault();
                        //     if(props.onClick)
                        //         props.onClick();
                        //     console.log('rrrrrr')
                        // }}
                        key={props.id}
                        nodeId={"f"+props.id}
                        label={
                            <div
                                style={{
                                    display: 'flex',
                                    flexDirection: 'row'
                                }}
                                onDoubleClick={(e)=>{
                                    e.preventDefault();
                                    handleDoubleClick();
                                }}
                                onContextMenu={(e)=>{
                                    e.preventDefault();
                                    handleClick(e);
                                }}
                            >
                                <DescriptionIcon style={{ color: '#ffffff' }}/>

                                    <Typography style={{
                                        userSelect : 'none',
                                        WebkitUserSelect: 'none',
                                        MozUserSelect: 'none',
                                        unselectable: 'on',
                                        color: '#ffffff'
                                    }}>{props.name}</Typography>

                            </div>
                        }
                    />
            }
            <Menu
                id="simple-menu"
                anchorEl={anchorEl}
                keepMounted
                open={Boolean(anchorEl)}
                onClose={handleClose}
            >
                {props.categories.map(el => {
                    i++;
                    return (<MenuItem onClick={handleClose} key={i}>{el}</MenuItem>);
                })}
            </Menu>
        </div>
    );
}

// export function FileItem(props){
//
//
//     return (
//
//         props.isDir===true ?
//             <div>
//                 <StyledTreeItem
//                     key={props.id}
//                     nodeId={"d"+props.id}
//                     label={
//                         props.children.length > 0 ?
//                             <div><Typography style={{ color: '#ffffff' }}>{props.name}</Typography></div>
//                             :
//                             <div style={{
//                                 display: 'flex',
//                                 flexDirection: 'row'
//                             }}>
//                                 <FolderOpenIcon style={{ color: '#ffffff' }} />
//                                 <Typography style={{ color: '#ffffff' }}>{props.name}</Typography>
//                             </div>
//                     }
//                 >
//
//                     {props.children}
//
//                 </StyledTreeItem>
//             </div>
//             :
//             <div>
//                 <StyledTreeItem
//                     onContextMenu={(e)=>{
//                         e.preventDefault();
//                         if(props.onClick)
//                             props.onClick();
//                         console.log('rrrrrr')
//                     }}
//                     key={props.id}
//                     nodeId={"f"+props.id}
//                     label={<div style={{
//                         display: 'flex',
//                         flexDirection: 'row'
//                     }}
//                     >
//                         <DescriptionIcon style={{ color: '#ffffff' }}/>
//                         <Typography style={{ color: '#ffffff' }}>{props.name}</Typography>
//                     </div>
//                     }
//                 />
//             </div>
//
//
//     )
// }

// // :
// {/*<div*/}
// {/*    style={{*/}
// {/*        display: 'flex',*/}
// {/*        flexDirection: 'row'*/}
// {/*    }}*/}
// {/*    onContextMenu={(e)=>{*/}
// {/*        e.preventDefault();*/}
// {/*        handleClick(e);*/}
// {/*    }}*/}
// {/*>*/}
// {/*    <FolderOpenIcon style={{ color: '#ffffff' }} />*/}
// {/*    <Typography style={{ color: '#ffffff' }}>{props.name}</Typography>*/}
//
// {/*</div>*/}





