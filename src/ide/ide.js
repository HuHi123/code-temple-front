import React from 'react';
import CustomizedTreeView from './dir';
import Editor from './editor';
import AppBar from '@material-ui/core/AppBar'
import Toolbar from '@material-ui/core/Toolbar'
import Typography from '@material-ui/core/Typography'
import IconButton from '@material-ui/core/IconButton'
import Button from '@material-ui/core/Button'
import {Resizable} from "re-resizable";
import PlayArrowIcon from '@material-ui/icons/PlayArrow';
import StopIcon from '@material-ui/icons/Stop';
import {TextField} from "@material-ui/core";
import TemporaryDrawer from "./chat"
import { useRef, useEffect } from "react";

import styled from 'styled-components';
const StyledTextField = styled(TextField)`
  label.focused {
    color: green; 💚
  }
  .MuiOutlinedInput-root {
    fieldset {
      border-color: black; 💔
    }
    &:hover fieldset {
      border-color: black; 💛
    }
    &.Mui-focused fieldset {
      border-color: black; 💚
    }
  }
  
`;

let l = null;
var ctrlDown = false,
    ctrlKey = 17,
    cmdKey = 91,
    vKey = 86,
    cKey = 67;



export default function Ide(props){


    const wrapperRef = useRef(null);
    const [open,setOpen] = React.useState(false);

    const toggleDrawer = (open) => event => {
        if (event.type === 'keydown' && (event.key === 'Tab' || event.key === 'Shift')) {
            return;
        }

        setOpen(open);
    };

    /**
     * Alert if clicked on outside of element
     */
    function handleClickOutside(event) {
        if (wrapperRef.current && !wrapperRef.current.contains(event.target)) {
            console.log("You clicked outside of me!");
            if(open)
                setOpen(false);
        }
    }

    useEffect(() => {
        document.addEventListener("mousedown", handleClickOutside);
        return () => {
            document.removeEventListener("mousedown", handleClickOutside);
        };
    });


    return(
        <div style={{
            width: '100%',
            height: '100%',
            display: 'flex',
            flexDirection: 'column',
        }}
        >
            <TemporaryDrawer  send={props.send} chat={props.chat} reff={wrapperRef} open={open} onClose={(state)=>{
                toggleDrawer(state)
            }}/>
            <div style={{

                flexGrow:1,
            }}>
                <AppBar position="static">

                    <Toolbar style={{
                        style: 'flex',
                        justifyContent: 'space-between'
                    }}>
                        <div style={{
                            display: 'flex',
                            flexDirection: 'row',
                            alignItems: 'center',
                        }}>
                            <Typography variant="h6">
                                The IDE
                            </Typography>
                            <IconButton
                                style={{
                                    marginLeft: '2em',
                                }}
                                color="inherit"
                                onClick={()=>{
                                    if(props.runState){
                                        if(props.stop)
                                            props.stop();
                                    }else{
                                        if(props.run)
                                            props.run();
                                    }

                                }}
                            >
                                {
                                    props.runState ?
                                        <StopIcon
                                            color="inherit"
                                        />
                                        :
                                        <PlayArrowIcon
                                            color="inherit"
                                            style={{
                                                cursor: 'pointer',
                                            }}
                                        />
                                }

                            </IconButton>

                        </div>
                        <div style={{
                            display: 'flex',flexDirection :'row',
                        }}>
                            <Button
                                color="inherit"
                                onClick={toggleDrawer(true)}
                            >Chat</Button>
                            <Button
                                color="inherit"
                                onClick={()=>{
                                    if(props.back)
                                        props.back();
                                }}
                            >Back</Button>
                        </div>

                    </Toolbar>
                </AppBar>
            </div>
            <div style={{
                flexGrow:5,
                display: 'flex',
                flexDirection: 'row',
                backgroundColor: '#3c3c3d',
            }}>
                <Resizable
                    style={{
                        minWidth:'200px',
                        display: 'flex',
                        flexDirection: 'column',
                        height: '90vh',
                        maxHeight: '90vh',
                        alignItems: 'stretch',

                    }}
                    defaultSize={{
                        width: 200,
                        // height: '900vh'
                    }}
                    enable={{ top:false, right:true, bottom:false, left:false, topRight:false, bottomRight:false, bottomLeft:false, topLeft:false }}
                >

                    <CustomizedTreeView
                        style={{
                            flexGrow: 7,
                            overflow: 'scroll',
                            maxHeight: '70%',
                            display: 'flex',
                            paddingBottom: '1em',
                        }}
                        id={props.id}
                        filesys={props.filesys}
                        createFile={props.createFile}
                        deleteFile={props.deleteFile}
                        renameFile={props.renameFile}
                        clickFile={(id)=>{
                            l =id;
                            props.clickFile(id)
                        }}
                    />
                    <StyledTextField
                        multiline
                        variant="outlined"
                        onKeyDown={(e)=> {
                            if (e.keyCode == ctrlKey || e.keyCode == cmdKey) {
                                ctrlDown = true;
                            }
                            else{
                                if (ctrlDown && (e.keyCode == vKey || e.keyCode == cKey)) {
                                    if (props.stop)
                                        props.stop();
                                } else {
                                    if (props.onKeyDown || e.keyCode!==16) {
                                        //props.onKeyDown('ll');
                                        let s = String.fromCharCode(e.which);
                                        if (e.shiftKey)
                                            props.onKeyDown(s);
                                        else
                                            props.onKeyDown(s.toLowerCase());

                                    }


                                }
                            }
                        }}
                        onKeyUp={(e)=>{
                            if (e.keyCode == ctrlKey || e.keyCode == cmdKey)
                                ctrlDown = false;
                        }}
                        style={{

                            flexGrow: 1,
                            overflowX: 'hidden',
                            overflowY: 'auto',
                            maxHeight: '30%',
                            backgroundColor: '#000',
                            // maxWidth: '100%',
                            // height:'100%',
                            minWidth: '0px',
                            minHeight: '30%',
                            borderWidth: '0px',
                            fullWidth:true,
                            '&:focus': {
                                outline: "none"
                            },


                        }}
                        value={props.stack}
                    />

                </Resizable>

                <Editor
                    new={props.new}
                    freeze={props.freeze}
                    initial={props.initial}
                    change={(value)=>{
                        props.change(value,l)
                    }}
                />

            </div>
        </div>
    );
}