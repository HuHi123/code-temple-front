import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import Button from '@material-ui/core/Button';
import List from '@material-ui/core/List';
import Divider from '@material-ui/core/Divider';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import InboxIcon from '@material-ui/icons/MoveToInbox';
import MailIcon from '@material-ui/icons/Mail';
import { ChatItem } from 'react-chat-elements'
import { MessageList } from 'react-chat-elements'
import 'react-chat-elements/dist/main.css';
import { Input } from 'react-chat-elements';
import TextField from "@material-ui/core/TextField";
import IconButton from "@material-ui/core/IconButton";
import SendIcon from '@material-ui/icons/Send';
import Card from "@material-ui/core/Card";



const useStyles = makeStyles({
    list: {
        // maxWidth: '40em',
    },
    fullList: {
        width: 'auto',
    },
});

export default function TemporaryDrawer(props) {
    const classes = useStyles();
    //props.open
    const [text,setText] = React.useState('');

    const messagesEnd = React.useRef(null);
    React.useEffect(()=>{
        if(props.open){
            setTimeout(()=>{
                messagesEnd.current.scrollIntoView({ behavior: "smooth" });
            },200)
        }


    },[props.open,props.chat]);



    const sideList = () => (
        <div
            className={classes.list}
            role="presentation"
            style={{
                width: '30em',//iu
                display:'flex',
                flexDirection: 'column',
                alignItems:'center',
                color: '#000',
                overflowY: 'hidden',
                overflowX: 'hidden',
                height: '100%',
            }}

            ref={props.reff}
        >
            <div style={{
                color:'black',
                width: '100%',
                height: '90%',
                overflowY: 'auto',
                MsOverflowStyle: 'none',
                WebkitScrollbar: {
                    display:'none'
                },

            }}
            >
                <MessageList
                    // toBottomHeight={'100%'}////(el) => { messagesEnd = el; }
                    downButton={true}
                    multiLine={true}
                    className='message-list'
                    lockable={true}
                    color='white'
                    textColor='black'
                    dataSource={props.chat} />
                <div style={{ float:"left", clear: "both" }}
                     ref={messagesEnd}>
                </div>
            </div>

            {/*</ThemeProvider>*/}
            <Card style={{
                display: 'flex',
                flexDirection: 'row',
                alignItems:'center',
                width: '100%',
                justifyContent: 'space-around',
                height: '10%',
                padding: '1em',
                backgroundColor: '#262626',
                overflowY: 'hidden',
                overflowX: 'hidden',

            }}
            >
                <div style={{
                    width: '90%',
                    overflowY: 'hidden',
                    overflowX: 'hidden',
                    display: 'flex',
                    flexDirection: 'row',
                    alignItems:'center',

                }}>
                <TextField
                    id="outlined-basic"
                    variant="outlined"
                    placeholder={'Type your message...'}
                    value={text}
                    onChange={(e)=>{
                        setText(e.target.value);
                    }}
                    style={{flexGrow:7,border: 0,marginRight: '0.5em',marginLeft: '0.5em'}}
                />
                <IconButton
                    disabled={text === ''}
                    variant="outlined"
                    onClick={()=>{
                        props.send(text);//message.projectId,message.title,message.text,message.date
                        setText('');

                    }}
                >
                    <SendIcon />
                </IconButton>
                </div>

            </Card>
        </div>
    );

    return (
        <div  onClick={(e)=>{
            e.stopPropagation();
        }}
              style={{
                  width:'70%'
              }}
        >
            {/*<Button onClick={()=>{*/}
            {/*    if(props.onClose)*/}
            {/*        props.onClose(true);//toggleDrawer( false)*/}
            {/*}}>Open Right</Button>*/}
            <Drawer   anchor="right" open={props.open} onClose={()=>{
                if(props.onClose)
                    props.onClose(false);//toggleDrawer( false)
            }}>
                {sideList('right')}
            </Drawer>
        </div>
    );
}