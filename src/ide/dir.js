import React from 'react';
import PropTypes from 'prop-types';
import {ThemeProvider,createMuiTheme, fade, makeStyles, withStyles} from '@material-ui/core/styles';
import TreeView from '@material-ui/lab/TreeView';
import TreeItem from '@material-ui/lab/TreeItem';
import Collapse from '@material-ui/core/Collapse';
import Typography from '@material-ui/core/Typography';
import { useSpring, animated } from 'react-spring/web.cjs'; // web.cjs is required for IE 11 support
import FolderOpenIcon from '@material-ui/icons/FolderOpen';
import FolderIcon from '@material-ui/icons/Folder';
import DescriptionIcon from '@material-ui/icons/Description';
import FileItem from './file';

import Slide from "@material-ui/core/Slide";
import {Button, TextField} from "@material-ui/core";
import Dialog from "@material-ui/core/Dialog";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogActions from "@material-ui/core/DialogActions";

const theme = createMuiTheme({
    palette: {
        type: 'dark',
        primary: {
            main: '#9EA2B3',
        },
    }
});

function TransitionComponent(props) {
    const style = useSpring({
        from: { opacity: 0, transform: 'translate3d(20px,0,0)' },
        to: { opacity: props.in ? 1 : 0, transform: `translate3d(${props.in ? 0 : 20}px,0,0)` },
    });

    return (
        <animated.div style={style}>
            <Collapse {...props} />
        </animated.div>
    );
}

TransitionComponent.propTypes = {
    /**
     * Show the component; triggers the enter or exit states
     */
    in: PropTypes.bool,
};

const StyledTreeItem = withStyles(theme => ({
    iconContainer: {
        '& .close': {
            opacity: 0.3,
        },
    },
    group: {
        marginLeft: 12,
        paddingLeft: 12,
        borderLeft: `1px dashed ${fade(theme.palette.text.primary, 0.4)}`,
    },
}))(props => <TreeItem {...props} TransitionComponent={TransitionComponent} />);

const useStyles = makeStyles({
    root: {
        flexGrow: 1,
    },
});

export default function CustomizedTreeView(props) {
    const classes = useStyles();

    const [popState,setPopState] = React.useState({
        open: false,
        type: false,
        action: false,
        default: false
    });

    let handleCreate=(parentId,name,type)=>{
        let projectId= props.id;let r = 'File';
        if(type ===2) r='Folder';
        console.log('created '+r+ ' named: '+name+ ' of parentId: '+parentId+ ' of projectId: '+projectId);
        if(props.createFile)
            props.createFile(parentId,name,type);
    };

    let handleDelete=(fileId,type)=>{
        let r = 'File';
        if(type ===2) r='Folder';
        console.log('deleted '+r+ ' of id: '+fileId);
        if(props.deleteFile)
            props.deleteFile(fileId,type);
    };

    let handleRename=(fileId,type,name)=>{
        let r = 'File';
        if(type ===2) r='Folder';
        console.log('rename '+r+ ' to name: '+name+ ' of id: '+fileId);
        if(props.renameFile)
            props.renameFile(fileId,type,name);
    };

    let handleFileClick=(id)=>{
        console.log('open '+id);
        if(props.clickFile)
            props.clickFile(id);
    };
    //FolderOpenIcon FolderIcon DescriptionIcon filesys.name
    function renderFile(filesys){
        //props: onChoose id name categories
        return (

            <FileItem
                id={filesys.id}
                name={filesys.name}
                doubleClick={()=>{
                    handleFileClick(filesys.id)
                }}
                categories={
                    filesys.isDir===true ?
                        (
                            filesys.id===0 ?
                                ['create file','create directory']//projectId
                                : ['create file','create directory','delete','rename']
                        )

                        :
                        ['delete','rename']
                }
                onChoose={(choice)=>{
                    if(filesys.isDir===true){
                        switch(choice){
                            case 'create file':
                                setPopState({
                                    open: true,
                                    type: 1,
                                    action: (name)=>{
                                        handleCreate(filesys.id,name,1);
                                    },
                                    default: ''
                                });
                                break;
                            case 'create directory':
                                setPopState({
                                    open: true,
                                    type: 1,
                                    action: (name)=>{
                                        handleCreate(filesys.id,name,2);
                                    },
                                    default: ''
                                });
                                break;
                            case 'delete':
                                setPopState({
                                    open: true,
                                    type: 2,
                                    action: ()=>{
                                        handleDelete(filesys.id,2)
                                    },
                                    default: false
                                });
                                break;
                            case 'rename':
                                setPopState({
                                    open: true,
                                    type: 3,
                                    action: (name)=>{
                                        handleRename(filesys.id,2,name)
                                    },
                                    default: filesys.name
                                });
                                break;
                        }
                    }else{
                        switch(choice){
                            case 'delete':
                                setPopState({
                                    open: true,
                                    type: 2,
                                    action: ()=>{
                                        handleDelete(filesys.id,1)
                                    },
                                    default: false
                                });
                                break;
                            case 'rename':
                                setPopState({
                                    open: true,
                                    type: 3,
                                    action: (name)=>{
                                        handleRename(filesys.id,1,name)
                                    },
                                    default: filesys.name
                                });
                                break;
                        }
                    }
                }}
                isDir={filesys.isDir}
            >
                {
                    filesys.children &&
                    filesys.children.map(el=>{
                        return renderFile(el);
                    })
                }
            </FileItem>

        )
    }
    let i=0;
    return (
        <ThemeProvider theme={theme}>

                <TreeView
                    className={classes.root}
                    defaultExpanded={['1']}
                    defaultCollapseIcon={<FolderOpenIcon onContextMenu={function(e){
                        e.preventDefault();
                    }} style={{ color: '#ffffff' }} />}
                    defaultExpandIcon={<FolderIcon onContextMenu={function(e){
                        e.preventDefault();
                    }} style={{ color: '#ffffff' }} />}
                    style={{
                        // minWidth:'200px',
                        //height: '90vh',
                        // height: '100%',
                        width: '100%',
                        // maxWidth: '200px',
                        // resize: 'horizontal',
                        overflowX: 'auto',
                        overflowY: 'auto',
                        WebkitOverflowScrolling: 'touch',
                        whiteSpace: 'nowrap',
                    }}
                    // defaultEndIcon={<DescriptionIcon />}
                >
                    {renderFile(props.filesys)}
                </TreeView>
            <Popup
                state={{
                    open: popState.open,
                    type: popState.type,
                    default: popState.default
                }}
                action={(name)=>{
                    popState.action(name);
                    setPopState({
                        open: false,
                        type: false,
                        action: false
                    })
                }}
                nothing={()=>{
                    setPopState({
                        open: false,
                        type: false,
                        action: false
                    })
                }}
            />
        </ThemeProvider>
    );
}






const Transition = React.forwardRef(function Transition(props, ref) {
    return <Slide direction="up" ref={ref} {...props} />;
});

export function Popup(props){


    const [text, setText] = React.useState(false);
    let t = props.state.default;
    const handleClickOpen = () => {
    };

    const handleClose = () => {
        if(props.action){
            if(props.state.type===1){
                if(text && text!==""){
                    props.action(text);
                }
            }

            if(props.state.type===2){
                props.action();
            }

            if(props.state.type===3){
                if(text && text!==""){
                    props.action(text);
                }
            }
        }

        setText(false);
    };

    const handleCloseNothing = () => {
        if(props.nothing)
            props.nothing();
        setText(false);
    };

    return (
        <div>
            <Dialog
                open={props.state.open}
                TransitionComponent={Transition}
                keepMounted
                onClose={handleClose}
                aria-labelledby="alert-dialog-slide-title"
                aria-describedby="alert-dialog-slide-description"
            >
                <DialogTitle id="alert-dialog-slide-title">{"Action"}</DialogTitle>
                <DialogContent>
                    <div style={{
                        display: 'flex',
                        flexDirection: 'column'
                    }}>
                        <DialogContentText id="alert-dialog-slide-description">
                            {
                                (()=>{
                                    if(props.state.type===1)
                                        return 'Choose a name:';

                                    if(props.state.type===2)
                                        return 'Are you sure you want to delete?';

                                    if(props.state.type===3)
                                        return 'Rename it to:';
                                })()
                            }
                        </DialogContentText>
                        {
                            (()=>{
                                if(props.state.type===1 || props.state.type===3){
                                    return (
                                        <TextField
                                        value={text===false ? props.state.default : text}
                                        onChange={(e)=>{
                                            setText(e.target.value);
                                        }}
                                    />
                                    );
                                }
                            })()
                        }

                    </div>
                </DialogContent>
                <DialogActions>
                    <Button onClick={handleClose} color="primary" disabled={(props.state.type!==2 && (text===false || text=== '' || text===props.state.default)) }>
                        Ok
                    </Button>
                    <Button onClick={()=>{
                        handleCloseNothing()
                    }} color="primary">
                        Cancel
                    </Button>
                </DialogActions>
            </Dialog>
        </div>
    );
}