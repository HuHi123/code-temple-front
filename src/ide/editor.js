// import {Controlled as CodeMirror} from 'react-codemirror2';
// import React from 'react';
// import 'codemirror/lib/codemirror.css';
// import 'codemirror/theme/material.css';
// import 'codemirror/theme/material-darker.css';
// import 'codemirror/theme/material-ocean.css';
// import 'codemirror/theme/midnight.css';
// import 'codemirror/theme/shadowfox.css'; //kk
// // import CodeMirror from 'react-codemirror2';
// require('codemirror/mode/xml/xml');
// require('codemirror/mode/javascript/javascript');



// require('codemirror/lib/codemirror.css');
// var React = require('react');
// var CodeMirror = require('react-codemirror');

import React from "react";
import AceEditor from "react-ace";
import "ace-builds/src-noconflict/mode-java";
import "ace-builds/src-noconflict/mode-javascript";
import "ace-builds/src-noconflict/theme-github";
import "ace-builds/src-noconflict/theme-twilight";

export default function Editor(props){
//props.freeze props.initial props.change(value)
    const [code,setCode] = React.useState('');

//
    return (
        <div style={{
            height: '100%',
            width:'100%',
            display: 'flex',
            overflowX: 'auto'
        }}>
            <AceEditor
                style={{
                    width: '100%',
                    height: '90vh',
                }}
                mode="javascript"
                theme="twilight"//(props.new ? props.initial : code)
                readOnly={!!props.freeze}
                value={props.freeze ? 'Double click A file to edit' : (props.new ? props.initial : code)}
                onChange={(value)=>{
                    console.log('ll '+value);
                    setCode(value)
                    if(props.change)
                        props.change(value);

                }}//
                name="UNIQUE_ID_OF_DIV"
                fontSize={props.freeze ? 70 : 20}
                wrapEnabled={true}
                onPaste={(e)=>{
                    console.log('pasted::: '+e.text);
                }}
                onCopy={(e)=>{
                    console.log('copied::: '+e.text);
                }}
                onCut={(e)=>{
                    console.log('cut::: '+e.text);
                }}
                enableBasicAutocompletion={true}
                placeholder={'Impress!!!'}
                editorProps={{ $blockScrolling: true }}
            />

        </div>
    );
}
