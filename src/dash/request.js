import {Button} from "@material-ui/core";
import {makeStyles}from '@material-ui/core/styles';
import Card from "@material-ui/core/Card";
import Typography from "@material-ui/core/Typography";
import React from "react";

export default function RequestCard(props){

    const classes = makeStyles(theme => ({
        req: {
            color: '#99ccff'
        },

    }))();
    return (
        <Card style={{
            display:'flex',
            flexDirection: 'column',
            alignItems: 'center',
            margin:'0.5em',
            padding:'0.5em',
            maxWidth: '17em',
        }} key={props.id}>
            <div style={{
                display:'flex',
                flexDirection: 'row',
                alignItems: 'center',
                flexWrap: 'wrap',
                width: '100%',
            }}>
                <Typography className={classes.req}>{props.user}</Typography>&nbsp;
                <Typography>{' whose'}</Typography>&nbsp;
                <Typography>{' email'}</Typography>&nbsp;
                <Typography className={classes.req}>{props.email}</Typography>&nbsp;
                <Typography>invited</Typography>&nbsp;
                <Typography>{' you to'}</Typography>&nbsp;
                <Typography>{' join'}</Typography>&nbsp;
                <Typography>{' the'}</Typography>&nbsp;
                <Typography>{' project '}</Typography>&nbsp;
                <Typography className={classes.req}>{props.project}</Typography>
            </div>
            <div style={{
                width: '100%',
                display:'flex',
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'space-around',
            }}>
                <Button
                    style={{
                        color: '#7b9a65',//678154
                    }}
                    onClick={()=>{
                        if(props.accept)
                            props.accept(props.id);
                    }}
                >Accept</Button>
                <Button
                    style={{
                        color: '#C75450',
                    }}
                    onClick={()=>{
                        if(props.reject)
                            props.reject(props.id);
                    }}
                >Reject</Button>
            </div>
        </Card>
    );
}