import React from 'react';
import Typography from '@material-ui/core/Typography';
import {createMuiTheme, makeStyles,ThemeProvider} from '@material-ui/core/styles';
import {Button} from "@material-ui/core";
import Card from "@material-ui/core/Card";
import RequestCard from "./request";
import ProjectAdd from "./projectAdd";
import ProjectCard from "./project";

//9EA2B3 8C769E #99ccff
const useStyles = makeStyles(theme => ({
    '@global': {
        body: {
            backgroundColor: '#3c3c3d',
        },
    },

    '@media': {
        b_row: { flexWrap: 'wrap' }
    }
}));

const theme = createMuiTheme({
    palette: {
        type: 'dark',
        primary: {
            main: '#fff',
        },
    },

});

export function Dashboard(props){

    const classes = useStyles();

    const [state,setState] = React.useState({
        drawer: false,
    });

    const [editMode,setEditMode] = React.useState(false);

    return(
        <ThemeProvider theme={theme}>
            <div>
                <Card style={{
                    width:'100%',
                    height: '4em',
                    display:'flex',
                    alignItems: 'center',
                    flexDirection: 'row',
                    justifyContent: 'space-between'
                }}>
                    <Typography  style={{marginLeft: '1em', fontSize:'1.5em'}}>Dashboard </Typography>
                    <div style={{
                        display:'flex',
                        flexDirection: 'row',
                        marginRight: '1em',
                    }}>

                        <ProjectAdd
                            projectAdd={(name)=>{
                                if(props.projectAdd)
                                    return props.projectAdd(name);
                            }}
                            disabled={editMode}
                        />


                        {
                            editMode ?
                                <Button
                                    onClick={()=>{
                                        setEditMode(false);
                                    }}
                                >
                                    Done
                                </Button>
                                :
                                <Button
                                    onClick={()=>{
                                        setEditMode(true);
                                    }}
                                >
                                    Edit
                                </Button>
                        }
                        <Button onClick={()=>{
                            if(props.switch)
                                props.switch('user');
                        }}>
                            Log Out
                        </Button>
                    </div>
                </Card>
                <div id={'b_row'} style={{
                    display: 'flex',
                    flexDirection: 'row',
                    justifyContent: 'space-around',
                    // flexWrap: 'wrap',
                }}>
                    <div style={{
                        flexGrow:4
                    }}>
                        <div>
                            <Typography color='primary'>Projects:</Typography>
                        </div>
                        <div style={{

                            flexWrap: 'wrap',
                            display: 'flex',
                            flexDirection: 'row'
                        }}>
                            {
                                props.projects.map(el=>{
                                    return <ProjectCard
                                        name={el.name}
                                        id={el.id}
                                        key={el.id}
                                        participants={el.participants}
                                        editMode={editMode}
                                        leave={props.leave}
                                        onNameChange={props.onNameChange}
                                        addParticipant={props.addParticipant}
                                        enterProject={props.enterProject}
                                    />
                                })
                            }
                        </div>
                    </div>

                    <div style={{
                        flexGrow:1
                    }}>
                        <div>
                            <Typography  color='primary'>Requests:</Typography>
                        </div>
                        <div>
                            {
                                props.req &&
                                props.req.map(el=>{
                                    return (
                                        <RequestCard
                                            user={el.user}
                                            email={el.email}
                                            project={el.name}
                                            id={el.id}
                                            accept={props.accept}
                                            reject={props.reject}
                                        />
                                    );
                                })
                            }
                        </div>
                    </div>
                </div>
            </div>
        </ThemeProvider>
    );
}
























