import React from 'react';
import Typography from '@material-ui/core/Typography';
import { makeStyles} from '@material-ui/core/styles';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import AddIcon from '@material-ui/icons/Add';
import {TextField} from "@material-ui/core";
import InputBase from "@material-ui/core/InputBase";
import DeleteIcon from '@material-ui/icons/Delete';


const useStyles = makeStyles(theme => ({
    root: {
        margin: '1em',
        width: '12em',
        maxWidth: '12em',

    },

    heading: {
        fontSize: theme.typography.pxToRem(15),
        fontWeight: theme.typography.fontWeightRegular,
    },
    expandIcon: {

    },
    '@global': {
        body: {
            backgroundColor: '#3c3c3d',
        },
    },
    req: {
        color: '#99ccff'
    },
    '@media': {
        b_row: { flexWrap: 'wrap' }
    }
}));

export default function ProjectCard(props){
    const [name,setName] = React.useState(props.name);
    const [state,setState] = React.useState({
        expanded: false,
        addParticipant: '',
    });

    React.useEffect(() => {
        setName(props.name);
    }, [props.name]);

    const classes = useStyles();

    return (
        <div className={classes.root} >
            <ExpansionPanel
                expanded={props.editMode? false : state.expanded}
                style={{
                    overflow: 'hidden',}}

            >
                <ExpansionPanelSummary
                    onClick={()=>{
                        if(!props.editMode && props.enterProject)
                            props.enterProject(props.id)
                    }}
                    expandIcon={ props.editMode ?
                        <DeleteIcon  onClick={(e)=>{
                            e.stopPropagation();
                            if(props.leave)
                                props.leave(props.id);
                        }} />
                        :
                        <ExpandMoreIcon onClick={(e)=>{
                            e.stopPropagation();
                            setState({...state,expanded:!state.expanded});
                        }} title={'expand'}/>
                    }
                    aria-controls="panel1a-content"
                    id="panel1a-header"
                >
                    {props.editMode ?
                        <InputBase
                            onKey
                            multiline variant="outlined"
                            style={{border:'0px'}} value={name}
                            onFocus={(e)=>{
                                e.preventDefault();
                                e.stopPropagation();
                            }
                            }
                            onChange={(e)=>{
                                if(e.target.value.charAt(e.target.value.length-1)!=='\n'){
                                    if(props.onNameChange){
                                        if(e.target.value==='')
                                            props.onNameChange(props.id,'Untitled');
                                        else
                                            props.onNameChange(props.id,e.target.value)
                                    }


                                    setName(e.target.value)
                                }

                            }}
                        />
                        :
                        (
                            ()=>{
                                if(name==='')
                                    setName('Untitled');
                                return <Typography className={classes.heading}>{name}</Typography>;
                            }
                        )()

                    }



                </ExpansionPanelSummary>
                <ExpansionPanelDetails style={{
                    display:'flex',
                    flexDirection: 'column'
                }}>
                    <Typography>
                        Participants:
                    </Typography>
                    {
                        props.participants.map(el=>{
                            return (
                                <div key={el.name}>
                                    <Typography style={{
                                        fontWeight: 700
                                    }} >
                                        {el.name}
                                    </Typography>
                                    <Typography style={{
                                        fontWeight: 300
                                    }}>
                                        {el.email}
                                    </Typography>
                                </div>
                            );
                        })
                    }
                    <div style={{
                        display: 'flex',
                        flexDirection: 'row',
                        justifyContent: 'space-around',
                        alignItems: 'center'
                    }}>
                        <TextField onChange={(e)=>{
                            setState({...state,addParticipant: e.target.value})
                        }} value={state.addParticipant}/>
                        <AddIcon style={{
                            cursor: 'pointer'
                        }}
                                 onClick={()=>{
                                     console.log('lfl'+state.addParticipant);
                                     if(props.addParticipant && state.addParticipant!=='')
                                         props.addParticipant(state.addParticipant,props.id);
                                     setState({...state,addParticipant:''})

                                 }}
                        />

                    </div>

                </ExpansionPanelDetails>
            </ExpansionPanel>
        </div>
    );

}