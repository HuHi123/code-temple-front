import React from 'react';
import {TextField,Button} from "@material-ui/core";
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Slide from '@material-ui/core/Slide';

const Transition = React.forwardRef(function Transition(props, ref) {
    return <Slide direction="up" ref={ref} {...props} />;
});

export default function ProjectAdd(props){

    const [open, setOpen] = React.useState(false);
    const [text, setText] = React.useState("");

    const handleClickOpen = () => {
        setOpen(true);
    };

    const handleClose = () => {
        if(props.projectAdd)
            props.projectAdd(text);
        setOpen(false);
        setText('');
    };

    return (
        <div>
            <Button color="primary" onClick={handleClickOpen} disabled={props.disabled}>
                ADD
            </Button>
            <Dialog
                open={open}
                TransitionComponent={Transition}
                keepMounted
                onClose={handleClose}
                aria-labelledby="alert-dialog-slide-title"
                aria-describedby="alert-dialog-slide-description"
            >
                <DialogTitle id="alert-dialog-slide-title">{"Create new project"}</DialogTitle>
                <DialogContent>
                    <div style={{
                        display: 'flex',
                        flexDirection: 'column'
                    }}>
                        <DialogContentText id="alert-dialog-slide-description">
                            Type the name of the new project:
                        </DialogContentText>
                        <TextField
                            value={text}
                            onChange={(e)=>{
                                setText(e.target.value);
                            }}
                        />
                    </div>
                </DialogContent>
                <DialogActions>
                    <Button disabled={text === ""}
                            onClick={handleClose}
                            color="primary">
                        Create
                    </Button>
                    <Button onClick={()=>{
                        setOpen(false);
                    }} color="primary">
                        Cancel
                    </Button>
                </DialogActions>
            </Dialog>
        </div>
    );
}