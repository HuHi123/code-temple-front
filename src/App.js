import React from 'react';
import './App.css';
import User from './user'
import ApiCaller from './ApiCaller'
import {Card} from '@material-ui/core';
import { createMuiTheme,ThemeProvider,makeStyles } from '@material-ui/core/styles';
import {Dashboard} from './dash/dashboard'
import io from 'socket.io-client';
import Ide from './ide/ide';
import Typography from "@material-ui/core/Typography";

let url = 'http://localhost:8000';
let api = new ApiCaller(url);
let token ;
let socket;
//djfa
function App() {

    const [pane, setPane] = React.useState("main");
    const [status,setStatus] = React.useState(false);
    const [custom,setCustom] = React.useState({});

    const classes = useStyles();
    let map ={
        user : <UserPane success={()=>{
            setPane('main');
        }} setStatus={(status)=>{setStatus(status)}}/>,
        main: <MainPane
            setStatus={(status)=>{
                setStatus(status)
            }}
            switch={(pane)=>{
                switchPane(pane)
            }}
            change={(obj)=>{
                changeCustom(obj);
            }}
        />,
        ide: <IdePane
            setStatus={(status)=>{
                setStatus(status)
            }}
            switch={(pane)=>{
                switchPane(pane)
            }}
            id={custom.id}
        />
    };

    function switchPane(pane){
        setPane(pane);
    }

    function changeCustom(obj){
        setCustom(obj);
    }

    return (
        <ThemeProvider theme={theme}>
            <div style={{

                backgroundColor: '#3c3c3d',
            }}>
                {
                    status &&

                    <Card className={classes.statusCard} >
                        {status}
                    </Card>

                }

                {
                    map[pane]
                }
            </div>
        </ThemeProvider>
    );
}

export default App;

function UserPane(props){

    return (

        <User
            onSwitch={()=>{props.setStatus(false)}}
            login={(email,password)=>{
                api.login(email,password)
                    .then(res=>{
                        console.log("login successful!!");
                        props.setStatus(false);
                        if(props.success)
                            props.success()
                    })
                    .catch(err=>{
                        console.log("cannot login!!");
                        props.setStatus('Incorrect password/email combination');
                    });
                props.setStatus('Logging...');

            }}

            signup={(name,email,password)=>{
                api.signup(name,email,password)
                    .then(res=>{
                        props.setStatus(false);
                        if(props.success)
                            props.success()
                    })
                    .catch(err=>{
                        console.log("cannot signup!!");
                        props.setStatus('Username Already in use!');
                    });
                props.setStatus('Signing up...');
            }}
        />

    );
}

function DashPane(props){
    const [state,setState] = React.useState(props.state);
    React.useEffect(()=>{
        socket = io(url+'/dashboard');
        token = ApiCaller.getToken();

        socket.on('update',(data)=>{
            updated(data);
        });

        socket.emit('join', { token: token });

        return ()=>{
            socket.close()
        }
    },[]);
    function updated(data){
        setState(data);
    }
    return (
        <Dashboard

            addParticipant={(participant,id)=>{//
                api.addParticipant(participant,id)
                    .then(res=>{
                        props.setStatus(false);
                    })
                    .catch(e=>{
                        props.setStatus('Failed to add participant...');
                        setTimeout(()=>{
                            props.setStatus(false)
                        },1000)
                    });
                props.setStatus('Adding participant...')
            }}

            onNameChange={(id,name)=>{//
                api.editProject(id,name)
                    .then(()=>{
                        socket.emit('update');
                    })
            }}

            leave={(id)=>{//
                api.deleteProject(id)
                    .then(res=>{
                        setState(res);
                        props.setStatus(false);
                        socket.emit('update');
                    })
                    .catch(err=>{
                        console.log('p[:'+err);
                        props.setStatus('Failed to delete project...');
                        setTimeout(()=>{
                            props.setStatus(false)
                        },1000)
                    });
                props.setStatus('Deleting Project...')
            }}

            projectAdd={(project)=>{//
                api.createProject(project)
                    .then(res=>{
                        setState(res);
                        props.setStatus(false);
                        socket.emit('update');
                    })
                    .catch(e=>{
                        props.setStatus('Couldn\'t create project...');
                        setTimeout(()=>{
                            props.setStatus(false)
                        },1000)
                    });
                props.setStatus('Creating Project...')
            }}

            projects={state.projects}

            req={state.requests}

            switch={(pane)=>{
                api.logout();
                if(props.switch)
                    props.switch(pane);
            }}

            accept={(id)=>{
                api.acceptProject(id)
                    .then(res=>{
                        setState(res);
                        props.setStatus(false);
                        socket.emit('update');
                    })
                    .catch(e=>{
                        props.setStatus('Failed to join project...');
                        setTimeout(()=>{
                            props.setStatus(false)
                        },1000)
                    });
                props.setStatus('Joining Project...');
            }}

            reject={(id)=>{
                api.rejectProject(id)
                    .then(res=>{
                        setState(res);
                        props.setStatus(false);
                        socket.emit('update');
                    })
                    .catch(e=>{
                        props.setStatus('Failed to reject...');
                        setTimeout(()=>{
                            props.setStatus(false)
                        },1000)
                    });
                props.setStatus('Rejecting invitation...')
            }}

            enterProject={(projectId)=>{
                console.log(projectId);
                props.change({id:projectId});
                props.switch('ide');
                // api.enterProject()
            }}
        />
    );

}

function MainPane(props){
    const [state,setState] = React.useState(null);

    React.useEffect(()=>{


        api.dashboard()
            .then(result=>{
                setState(result);

            })
            .catch((e)=>{
                if(props.switch)
                    props.switch('user');
            })


    },[]);

    function updated(data){
        setState(data);
    }

    return (
        state &&
        <DashPane
            state={state}
            setStatus={props.setStatus}
            switch={props.switch}
            change={props.change}
        />
    );
}

let socket2 = {emit:()=>{},removeAllListeners:()=>{},close:()=>{}};
let socket3 = {emit:()=>{},removeAllListeners:()=>{},close:()=>{}};

function IdePane(props){


    const [ideState,setIdeState] = React.useState({
        id:null,
        freeze: true,
        initial: '',
        new: false,
    });
    const [files,setFiles] = React.useState(false);
    const [runState,setRunState] = React.useState(false);
    const [stack,setStack] = React.useState('Output:\n');
    const [chat,setChat] = React.useState([]);

    React.useEffect(()=>{
        if(ideState.id !=null) {
            socket2 = io(url+'/file');
            const token = ApiCaller.getToken();
            socket2.emit('join', {token: token, fileId: ideState.id});
            socket2.on('FileUpdate', (content) => {
                console.log('yess');
                setIdeState({
                    id: ideState.id,
                    freeze: false,
                    initial: content,
                    new: true
                })
            });
        }
        return ()=>{
            socket2.close();
        }

    },[ideState.id]);

    React.useEffect(()=>{
        socket3 = io(url+'/tree');
        const token = ApiCaller.getToken();
        socket3.emit('join', {token: token, projectId: props.id});

        socket3.on('TreeUpdate', (res) => {
            setFiles(res.files)
        });

        socket3.on('stack', (data) => {
            setStack(stack=>{return stack+'\n'+data;});
        });

        socket3.on('termination', (res) => {
            console.log('terminate: '+res);
            setRunState(false);
        });

        socket3.on('message', (res) => {
            console.log('message: '+JSON.stringify(chat));
            setChat(chat=>chat.concat({...res,date:new Date(new Date(res.date).toLocaleString()+' UTC')}));
        });


        api.enterProject(props.id)
            .then(res => {
                setFiles(res.files);
                res.messages.forEach(m=>{
                    m.date=  new Date(new Date(m.date).toLocaleString()+' UTC');
                });
                setChat(res.messages);
            })
            .catch(() => {
                props.setStatus('Failed to enter the project...');
                setTimeout(() => {
                    props.setStatus(false)
                }, 1000);
                props.switch('main');
            });
        return ()=>{
            socket3.close();
        }
    },[]);


    /*

     */
    return (
        files ?
            <Ide
                runState={runState}
                stack={stack}
                id={props.id}
                new={ideState.new}
                filesys={files}
                chat={chat}

                send={(text)=>{
                    setChat(chat.concat({
                        projectId: props.id,
                        title: 'You',
                        text: text,
                        date: new Date(),
                        position: 'right',
                        textColor:'black',
                        type: 'text',
                    }));
                    socket3.emit('message',ApiCaller.getToken(),{
                        projectId: props.id,
                        text: text,
                        date: new Date()
                    });
                }}

                onKeyDown={(text)=>{
                    socket3.emit('write',text);
                }}

                run={()=>{
                    socket3.emit('run',{
                        token:ApiCaller.getToken(),
                        projectId: props.id
                    });
                    setStack('Output:\n');
                    setRunState(true);
                }}

                stop={()=>{
                    socket3.emit('stop');
                }}

                back={()=>{
                    props.switch('main');
                }}

                createFile={(parentId,name,type)=>{
                    api.newFile(props.id,parentId,name,type)
                        .then(res=>{
                            console.log("9090\n"+JSON.stringify(res.files));
                            setFiles(res.files);
                            props.setStatus(false);
                        })
                        .catch(e=>{
                            props.setStatus('Failed adding file: '+name);
                            setTimeout(()=>{
                                props.setStatus(false)
                            },1000)
                        });
                    props.setStatus('Adding file...');
                }}

                deleteFile={(fileId,type)=>{
                    api.deleteFile(fileId,type)
                        .then(res=>{
                            console.log("9090\n"+JSON.stringify(res.files));
                            setFiles(res.files);
                            props.setStatus(false);
                        })
                        .catch(e=>{
                            props.setStatus('Failed deleting file');
                            setTimeout(()=>{
                                props.setStatus(false)
                            },1000)
                        });
                    props.setStatus('Deleting file...');
                }}

                renameFile={(fileId,type,name)=>{
                    let projectId =false;
                    if(fileId===0)
                        projectId = props.id;
                    api.renameFile(fileId,type,name,projectId)
                        .then(res=>{
                            console.log("9090\n"+JSON.stringify(res.files));
                            setFiles(res.files);
                            props.setStatus(false);
                        })
                        .catch(e=>{
                            console.log("oooooo"+e.message)
                            props.setStatus('Failed renaming file');
                            setTimeout(()=>{
                                props.setStatus(false)
                            },1000)
                        });
                    props.setStatus('Renaming file...');
                }}

                clickFile={(id)=>{
                    api.getFile(id)
                        .then(res=>{
                            console.log('koko+ '+res );
                            setIdeState({
                                id: id,
                                freeze: false,
                                initial: res,
                                new: true
                            })
                        })
                        .catch(err=>{

                        })
                }}

                newFile={(projectId,parentId,name,type)=>{
                    api.newFile(projectId,parentId,name,type)
                        .then(res=>{
                            console.log("9090\n"+JSON.stringify(res.files));
                            setFiles(res.files);
                            props.setStatus(false);
                        })
                        .catch(e=>{
                            props.setStatus('Failed adding file: '+name);
                            setTimeout(()=>{
                                props.setStatus(false)
                            },1000)
                        });
                    props.setStatus('Adding file...');
                }}

                freeze={ideState.freeze}

                initial={ideState.initial}

                change={(value,id)=>{
                    setIdeState({
                        ...ideState,
                        new: false
                    });
                    console.log('d::'+value+' on:'+id);
                    socket2.emit('FileUpdate',value);
                }}
            />
            :
            <Typography variant={'h3'} style={{
                color: '#fff'
            }}>Fetching...</Typography>
    );
}

const useStyles = makeStyles({
    statusCard: {
        position:'absolute',
        paddingTop: '0.3em',
        paddingBottom: '0.3em',
        paddingRight: '0.5em',
        paddingLeft: '0.5em',
        borderTopRightRadius: 0,
        borderTopLeftRadius: 0,
        top: '0px',
        left: '50%',
        transform : 'translate(-50%)',
        zIndex:4,
    },
    '@global': {
        body: {
            backgroundColor: '#3c3c3d',
        },
    },
});
const theme = createMuiTheme({
    palette: {
        type: 'dark',
        primary: {
            main: '#9EA2B3',
        },
    },

});
